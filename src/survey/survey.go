package survey

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var MongoDatabase *mgo.Session

var QuestionDatabase *sql.DB

var SessionList = make(map[string]map[string][]string)

func init() {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		"admin", "FluxPostPass", "flux_survey", "104.236.12.76", "5432")
	db, err := sql.Open("postgres", dbinfo)

	if err != nil {
		log.Fatal(err)
	}
	QuestionDatabase = db
	
}

func Load_Survey_Database() {
	session, err := mgo.Dial("104.236.12.76")
	if err != nil {
		log.Fatal(err)
	}

	MongoDatabase = session

	//defer session.Close()
}

func Get_Survey_Question(question string) []map[string]interface{} {
	query := fmt.Sprintf("SELECT * FROM question_parent_child JOIN question ON question.id = question_parent_child.child WHERE question_parent_child.parent='%v';", question)
	rows, err := QuestionDatabase.Query(query)
	if err != nil {
		log.Fatal(err)
	}

	cols, _ := rows.Columns()

	var mapList []map[string]interface{}

	for rows.Next() {
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i, _ := range columns {
			columnPointers[i] = &columns[i]
		}

		// Scan the result into the column pointers...
		if err := rows.Scan(columnPointers...); err != nil {

		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		m := make(map[string]interface{})
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			m[colName] = *val
		}

		// Outputs: map[columnName:value columnName2:value2 columnName3:value3 ...]
		fmt.Print(m)
		mapList = append(mapList, m)
	}

	return mapList
}

func Process_Task_Answer(answer []string, question []map[string]interface{}, originalQuestion string, session string) (response string) {
	c := MongoDatabase.DB("intro_survey").C("sessions")
	result := make(map[string]interface{})

	SessionList[session]["tasks"] = answer
	
	err := c.Find(bson.M{"sessionId": session}).One(&result)
	if err != nil {
		fmt.Println(err)
	}

	body := `{"Main":"Perfect! We have one last thing to get started","Body":"What is the name of your project?","id":5}`
	b := []byte(body)

	var f map[string]interface{}
	err = json.Unmarshal(b, &f)
	if err != nil {
		log.Fatal(err)
	}
	out, _ := json.Marshal(f)
	response = string(out)
	fmt.Sprintln(response)

	colQuerier := bson.M{"sessionId": session}
	change := bson.M{"$set": bson.M{fmt.Sprintf("Question_%v", originalQuestion): bson.M{"id": 5, "answer": "", "response": 4}}}
	err = c.Update(colQuerier, change)
	if err != nil {
		fmt.Println(err)
	}

	return response
}

func Process_Intro_Answer(answer string, question []map[string]interface{}, originalQuestion string, session string) (response string) {
	c := MongoDatabase.DB("intro_survey").C("sessions")
	result := make(map[string]interface{})
	
	err := c.Find(bson.M{"sessionId": session}).One(&result)
	if err != nil {
		fmt.Println(err)
	}

	switch originalQuestion {
	case "1":
		body := fmt.Sprintf(`{"Main":"Thank you %v! Now we can get started.","Body":"What is the vision for your project?","id":2}`, answer)
		b := []byte(body)

		var f map[string]interface{}
		err = json.Unmarshal(b, &f)
		if err != nil {
			log.Fatal(err)
		}
		out, _ := json.Marshal(f)
		response = string(out)
		fmt.Sprintln(response)

		SessionList[session]["name"] = []string{answer}

		colQuerier := bson.M{"sessionId": session}
		change := bson.M{"$set": bson.M{fmt.Sprintf("Question_%v", originalQuestion): bson.M{"id": 1, "answer": answer, "response": 2}}}
		err = c.Update(colQuerier, change)
		if err != nil {
			fmt.Println(err)
		}
	case "2":
		//TODO Add a keyword search to find the related skills for the term.
		body := fmt.Sprintf(`{"Main":"Here's your vision: %v.","Body":"What kind of project are you building?","id":3}`, answer)
		b := []byte(body)
		SessionList[session]["vision"] = []string{answer}
		
		
		var f map[string]interface{}
		err = json.Unmarshal(b, &f)
		if err != nil {
			log.Fatal(err)
		}
		out, _ := json.Marshal(f)
		response = string(out)
		fmt.Sprintln(response)

		colQuerier := bson.M{"sessionId": session}
		change := bson.M{"$set": bson.M{fmt.Sprintf("Question_%v", originalQuestion): bson.M{"id": 2, "answer": answer, "response": 3}}}
		err = c.Update(colQuerier, change)
		if err != nil {
			fmt.Println(err)
		}
	case "3":
		body := fmt.Sprintf(`{"Main":"Your working on a %v.","Body":"What tasks have you already done?","id":4}`, answer)
		b := []byte(body)
		SessionList[session]["type"] = []string{answer}
		var f map[string]interface{}
		err = json.Unmarshal(b, &f)
		if err != nil {
			log.Fatal(err)
		}
		out, _ := json.Marshal(f)
		response = string(out)
		fmt.Sprintln(response)

		colQuerier := bson.M{"sessionId": session}
		change := bson.M{"$set": bson.M{fmt.Sprintf("Question_%v", originalQuestion): bson.M{"id": 2, "answer": answer, "response": 3}}}
		err = c.Update(colQuerier, change)
		if err != nil {
			fmt.Println(err)
		}
	}

	return response
}

func store_question_response(originalQuestion string, answer string, session string, id int, response int) {
	//TODO Make c an argument
	c := MongoDatabase.DB("intro_survey").C("sessions")
	colQuerier := bson.M{"sessionId": session}
	change := bson.M{"$set": bson.M{fmt.Sprintf("Question_%v", originalQuestion): bson.M{"id": id, "answer": answer, "response": response}}}
	err := c.Update(colQuerier, change)
	if err != nil {
		fmt.Println(err)
	}
}
