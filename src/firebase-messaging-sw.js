// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
var config = {
    apikey: "AAAAskOw0U8:APA91bHWWqwO1AF_dq1MkXwpMdawR7aIx3-0kpTsrWNm01WUopc8mfbDm6JVee_Q1naJIbMQXo08Bxpc-i3kzlQRRq5954oFBUT1zZqby3U9ciQvqZjKfw4JRd1Z7N869la6Nj4gdSPJ",
    messagingSenderId: "765639840079",   
}
firebase.initializeApp(config);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

// messaging.setBackgroundMessageHandler(function (payload) {
//     console.log("Got it")
//     return self.registration.showNotification('payload')
// });