package local_database

import (
	"log"
	//"os"
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

var MainDatabase *sql.DB

func Connect_To_Test_Database() {
	// Create SQLite DB
	//var database_name = name
	//os.Remove(database_name)

	//db, err := sql.Open("sqlite3", database_name)
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		"admin", "FluxPostPass", "flux_main", "104.236.12.76", "5432")
	db, err := sql.Open("postgres", dbinfo)

	if err != nil {
		log.Fatal(err)
	}

	// Only close DB when main is finished
	// createUserTable := `
	// 	CREATE TABLE authentication_users (id integer NOT NULL PRIMARY KEY, username text, password text, email text, displayName text, UNIQUE (id), UNIQUE (email));

	// `
	// _, error := db.Exec(createUserTable)
	// if error != nil {
	// 	//Use for debugging purposes
	// 	//log.Printf("%q: %s\n", error, createUserTable)
	// }
	MainDatabase = db
}

// May only need to exist for testing; in reality a Postgresql DB will be used with consistant connection
// func test_database_presence(database_name string){
// 	//db_test_open, error_test := os.OpenFile(database_name, os.O_RDONLY|os.O_CREATE, 0666) // Creates the database if none exists
// 	db, err := sql.Open("sqlite3", database_name)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer db.Close()

// 	// if error_test != nil {
// 	// 	log.Fatal(error_test)
// 	// }

// 	//db_test_open.Close() // Close out the test
// }
