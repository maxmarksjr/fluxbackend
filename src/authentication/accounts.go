package authentication

import (
	"fmt"
	"log"

	db "../local_database"
	"golang.org/x/crypto/bcrypt"
)

// Defines basic user struct with JSON serialization identifiers
type User struct {
	Email       string `json:"email"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	DisplayName string `json:"display_name"`
}

// func main() {
//     for {

//         // Enter a password and generate a salted hash
//         pwd := getPwd()
//         hash := hashAndSaltPassword(pwd)

//         // Enter the same password again and compare it with the
//         // first password entered
//         pwd2 := getPwd()
//         pwdMatch := comparePasswords(hash, pwd2)

//         fmt.Println("Passwords Match?", pwdMatch)

//     }
// }

func CreateUser(e string, u string, p string, dn string, databaseName string) (bool, int) {
	database := db.MainDatabase
	rows, err := database.Query(fmt.Sprintf("SELECT email FROM authentication_users WHERE email='%v';", e))
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var count = 0
	for rows.Next() {
		count += 1
	}

	if count > 0 {
		return false, 0
	}

	password := hashAndSaltPassword([]byte(p))
	insert_row, error := database.Query(fmt.Sprintf("INSERT INTO authentication_users(username, password, email, displayName) VALUES ('%v', '%v', '%v', '%v') RETURNING id;", u, password, e, dn))
	if error != nil {
		log.Fatal(error)
	}
	var id int 
	for insert_row.Next() {
		err = insert_row.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
	}

	return true, id
}

func LoginUser(email string, password string) (bool, int) {
	database := db.MainDatabase
	rows, err := database.Query(fmt.Sprintf("SELECT id, password FROM authentication_users WHERE email='%v';", email))
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var count = 0
	var pass string
	var id int
	for rows.Next() {
		count += 1
		if err := rows.Scan(&id,&pass); err != nil {
			log.Fatal(err)
		}
	}

	if count <= 0 {
		return false, 0
	}
	fmt.Printf("HELLO WORLD\t\t%v\n", id)
	fmt.Println([]byte(password))
	return comparePasswords(pass, []byte(password)), id
}

//https://medium.com/@jcox250/password-hash-salt-using-golang-b041dc94cb72
func GetPwd() []byte {

	// Prompt the user to enter a password
	fmt.Println("Enter a password")

	// We will use this to store the users input
	var pwd string

	// Read the users input
	_, err := fmt.Scan(&pwd)
	if err != nil {
		log.Println(err)
	}

	// Return the users input as a byte slice which will save us
	// from having to do this conversion later on
	return []byte(pwd)

}

func hashAndSaltPassword(pass []byte) string {
	// Hashes password using the bcrypt library; uses minimal salt hashing
	hash, err := bcrypt.GenerateFromPassword(pass, bcrypt.MinCost)

	// Checks for hashing error
	if err != nil {
		log.Println("System Error: Password Hashing Failed")
	}

	return string(hash)
}

func comparePasswords(hashed string, pass []byte) bool {

	byteHash := []byte(hashed)

	err := bcrypt.CompareHashAndPassword(byteHash, pass)
	if err != nil {
		log.Println("Authentication Error: Password Attempt Failed")
		return false
	}

	return true
}
